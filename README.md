# Gists Viewer

A simple Github gist viewer written in angular4.


## Installation

```bash
npm install
```

## Usage

```bash
npm start
```
Navigate to `http://localhost:4200/` and use any gist ID or given examples.

Tested on `macOS 10.12.5` with `Node.JS v8.2.1` and `npm 5.3.0` on `Chrome 59.0.3071.115`.