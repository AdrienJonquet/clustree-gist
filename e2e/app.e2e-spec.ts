import { GistsPage } from './app.po';

describe('gists App', () => {
  let page: GistsPage;

  beforeEach(() => {
    page = new GistsPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
