
// Ce model a été généré à l'aide du site http://json2ts.com
// depuis un JSON récupéré depuis CURL
// puis complété =>  [key: string] car le champs est dyamique pour un fichier

export type File = {
  filename: string
  type: string
  language: string
  raw_url: string
  size: number
  truncated: boolean
  content: string
}

export type Files = {
  [key: string]: File
}

export type User = {
  login: string
  id: number
  avatar_url: string
  gravatar_id: string
  url: string
  html_url: string
  followers_url: string
  following_url: string
  gists_url: string
  starred_url: string
  subscriptions_url: string
  organizations_url: string
  repos_url: string
  events_url: string
  received_events_url: string
  type: string
  site_admin: boolean
}

export type ChangeStatus = {
  total: number
  additions: number
  deletions: number
}

export type History = {
  user: User
  version: string
  committed_at: Date
  change_status: ChangeStatus
  url: string
}

export type Gist = {
  url: string
  forks_url: string
  commits_url: string
  id: string
  git_pull_url: string
  git_push_url: string
  html_url: string
  files: Files
  public: boolean
  created_at: Date
  updated_at: Date
  description: string
  comments: number
  user?: any
  comments_url: string
  forks: any[]
  history: History[]
  truncated: boolean
}


export type Comment = {
  url: string
  id: number
  user: User
  created_at: Date
  updated_at: Date
  body: string
}


export module GistUtils {
  export function files(gist: Gist): File[] {
    if (gist != null && gist.files != null) {
      const fileNames: string[] = Object.getOwnPropertyNames(gist.files)
      return fileNames.map(f => {
        const file: File = gist.files[f]
        return file
      })
    } else return []
  }
}