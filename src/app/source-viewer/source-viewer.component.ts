import { Component, OnInit, Input } from '@angular/core'
import { File } from '../models/github.model'


@Component({
  selector: 'source-viewer',
  templateUrl: './source-viewer.component.html',
  styleUrls: ['./source-viewer.component.css']
})
export class SourceViewerComponent implements OnInit {

  @Input() source: File[]

  constructor() { }

  ngOnInit() {
  }

}
