import { Component, OnInit, Input } from '@angular/core'
import { Comment } from '../models/github.model'

@Component({
  selector: 'comment-viewer',
  templateUrl: './comment-viewer.component.html',
  styleUrls: ['./comment-viewer.component.css']
})
export class CommentViewerComponent implements OnInit {

  @Input() comment: Comment[]

  constructor() { }

  ngOnInit() {
  }

}
