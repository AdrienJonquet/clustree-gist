import { Component, OnInit } from '@angular/core'
import { GithubService } from '../services/github.service'
import { Gist, GistUtils, File, Comment } from '../models/github.model'

@Component({
  selector: 'app-gist-viewer',
  templateUrl: './gist-viewer.component.html',
  styleUrls: ['./gist-viewer.component.css']
})
export class GistViewerComponent implements OnInit {

  constructor(public githubService: GithubService) { }

  isLoading: boolean = false
  gistId: string
  gist: Gist
  files: File[] = []
  comments: Comment[] = []
  errorMsg: string

  ngOnInit() { }

  gistIdChange() {
    if (this.gistId != null) {
      this.isLoading = true
      this.errorMsg = ''
      this.githubService.getGist(this.gistId).subscribe(
        (gist: Gist) => {
          this.gist = gist
          this.files = GistUtils.files(this.gist)
          this.githubService.getGistComments(this.gistId).subscribe(
            (comments: Comment[]) => {
              this.isLoading = false
              this.comments = comments
              console.log(this.comments)
            },
            error => {
              this.isLoading = false
              this.errorMsg = error
            }
          )
        },
        error => {
          this.isLoading = false
          this.errorMsg = error
        }
      )
    }
  }

}
