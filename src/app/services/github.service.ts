import { Injectable, NgZone } from '@angular/core'
import { Http, Headers, Response } from '@angular/http'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/map'

import { Gist, Comment } from '../models/github.model'

@Injectable()
export class GithubService {

  private url = 'https://api.github.com'

  constructor(private http: Http) { }

  getGist(id: string): Observable<Gist> {
    return this.http
      .get(`${this.url}/gists/${id}`)
      .map(res => res.json())
  }

  getGistComments(id: string): Observable<Comment[]> {
    return this.http
      .get(`${this.url}/gists/${id}/comments`)
      .map(res => res.json())
  }
}