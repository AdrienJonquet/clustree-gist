import { Pipe, PipeTransform } from '@angular/core'
import { DomSanitizer } from '@angular/platform-browser'

import { markdown } from 'markdown'

@Pipe({ name: 'sanitizeHtml', pure: false })
export class SanitizeHtmlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {
  }
  transform(content) {
    return this.sanitizer.bypassSecurityTrustHtml(content)
  }
}


@Pipe({ name: 'markdown', pure: false })
export class MarkdownPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {
  }
  transform(content) {
    return markdown.toHTML(content)
  }
}
