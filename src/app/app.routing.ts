import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GistViewerComponent } from './gist-viewer/gist-viewer.component'



const appRoutes: Routes = [
  {
    path: 'gist',
    component: GistViewerComponent
  },

  {
    path: '',
    redirectTo: 'gist',
    pathMatch: 'full'
  },
];

export const authProviders = [];

export const appRoutingProviders: any[] = [];

export const routing: ModuleWithProviders = RouterModule.forRoot(appRoutes)