import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'
import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'
import { routing, appRoutingProviders } from './app.routing'

import * as hljs from 'highlight.js';
import { HighlightJsModule, HIGHLIGHT_JS } from 'angular-highlight-js'

import { AlertModule } from 'ngx-bootstrap'
import { ButtonsModule } from 'ngx-bootstrap/buttons'

import { GithubService } from './services/github.service'
import { SanitizeHtmlPipe, MarkdownPipe } from './pipes/html.pipe'

import { AppComponent } from './app.component'
import { GistViewerComponent } from './gist-viewer/gist-viewer.component'

import { CommentViewerComponent } from './comment-viewer/comment-viewer.component'
import { SourceViewerComponent } from './source-viewer/source-viewer.component'

export function highlightJsFactory() {
  return hljs;
}

@NgModule({
  declarations: [
    AppComponent,
    GistViewerComponent,
    SanitizeHtmlPipe,
    MarkdownPipe,
    CommentViewerComponent,
    SourceViewerComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HighlightJsModule,
    routing,
    HighlightJsModule.forRoot({
      provide: HIGHLIGHT_JS,
      useFactory: highlightJsFactory
    }),
    AlertModule.forRoot(),
    ButtonsModule.forRoot(),
  ],
  providers: [
    appRoutingProviders,
    GithubService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
